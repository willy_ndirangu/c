/*
 * @Author: Wilson Ndirangu 
 * @Date: 2020-01-09 20:41:22 
 * @Last Modified by: Wilson Ndirangu
 * @Last Modified time: 2020-01-09 21:27:42
 */

#include "Cube.h"


namespace  uuic{

double Cube::getVolume(){

    return length_ * length_ * length_;
}
void Cube::setLength(double length){
    Cube::length_=length;
}

double Cube::getSurfaceArea(){

    return 6*length_*length_;
}
}