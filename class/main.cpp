/*
 * @Author: Wilson Ndirangu 
 * @Date: 2020-01-09 20:43:20 
 * @Last Modified by: Wilson Ndirangu
 * @Last Modified time: 2020-01-09 21:28:42
 */
#include <iostream>
#include "Cube.h"

int main()
{

    uuic::Cube c;
    c.setLength(3.0);
    double volume= c.getVolume();
    double surfaceArea= c.getSurfaceArea();

    std::cout <<volume<< std::endl;
    std::cout <<surfaceArea<< std::endl;

    return 0;


}