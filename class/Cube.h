/*
 * @Author: Wilson Ndirangu 
 * @Date: 2020-01-09 20:38:38 
 * @Last Modified by: Wilson Ndirangu
 * @Last Modified time: 2020-01-09 21:24:28
 */
#pragma once

namespace uuic {

    class Cube
    {
    private:
        /* data */
        double length_;
    public:
       
        double getVolume();
        double getSurfaceArea();
        void setLength(double length);

    };
    

    
}